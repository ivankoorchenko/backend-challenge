package global.vigil

import scala.io.StdIn

object BackendChallengeApp {
  def main(args: Array[String]): Unit = {
    println("This is text adjustment application. You can format text to have lines no longer then N")

    val text = StdIn.readLine("Type a text to adjust")
    println("Type text max line length")
    val maxLength = StdIn.readInt()

    val adjustedText = AdjustText.adjustText(text, maxLength)
    println("Adjusted text: ")
    println(adjustedText)
  }
}
