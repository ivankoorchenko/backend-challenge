package global.vigil

import scala.collection.immutable.Queue

object AdjustText {

  /**
   * Utility function to adjust incoming text, so each line won't be longer then given line length.
   *
   * This function operates with notion of words and lines, so let's clarify them below:
   * - word - part of text divided with whitespace symbol. For instance: `Hello, World!`. Words: `Hello,` and `World!`
   * - line - part of text divided with `\n` (new line) symbol.
   *
   * No words will be broken between line and this has more priority over adjusting text lines.
   * Important consequence of this is - in case if there will found a word longer then given max line length,
   * then it will occupy whole line and line length will exceed.
   *
   * Any existing lines in the text which length already smaller then given one will preserve.
   * Any extra whitespace between words will be removed.
   *
   * @param text text to adjust
   * @param lineLength max line length to be applied
   * @return resulting text
   */
  def adjustText(text: String, lineLength: Int): String = {
    def splitOnLines(parsedLine: Array[String]): List[String] = {
      val zero = Queue.empty[String] -> Option.empty[String]
      val (lines, currentLine) = parsedLine.foldLeft(zero) {
        case ((lines, None), word) if word.length >= lineLength =>
          lines.enqueue(word) -> None

        case ((lines, None), word) =>
          lines -> Some(word)

        case ((lines, Some(currentLine)), word) if currentLine.length + word.length < lineLength  =>
          lines -> Some(s"$currentLine $word")

        case ((lines, Some(currentLine)), word) =>
          lines.enqueue(currentLine) -> Some(word)
      }
      currentLine.map(lines.enqueue).getOrElse(lines).toList
    }

    def adjustLine(line: String): List[String] = {
      val parsedLine = line.split(' ').map(_.trim).filter(_.nonEmpty)
      val parsedLineLength = parsedLine.map(_.length).sum
      if(parsedLineLength <= lineLength) {
        List(parsedLine.mkString(" "))
      } else {
        splitOnLines(parsedLine)
      }
    }

    text
      .split('\n')
      .flatMap(adjustLine)
      .mkString("\n")
  }
}
