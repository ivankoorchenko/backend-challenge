package global.vigil

class AdjustTextSuite extends munit.FunSuite {
  test("should adjust task text") {
    val text = "In 1991, while studying computer science at University of Helsinki, Linus Torvalds began a project that later became the Linux kernel. He wrote the program specifically for the hardware he was using and independent of an operating system because he wanted to use the functions of his new PC with an 80386 processor. Development was done on MINIX using the GNU C Compiler."
    val actualResult = AdjustText.adjustText(text, 40)
    val expectedResult =
      s"""In 1991, while studying computer science
         |at University of Helsinki, Linus
         |Torvalds began a project that later
         |became the Linux kernel. He wrote the
         |program specifically for the hardware he
         |was using and independent of an
         |operating system because he wanted to
         |use the functions of his new PC with an
         |80386 processor. Development was done on
         |MINIX using the GNU C Compiler.""".stripMargin

    assertEquals(actualResult, expectedResult)
  }


  test("should adjust task text with extra space") {
    val text = "In 1991,             while     studying computer science at University  of  Helsinki,    Linus    Torvalds began a project that later became the Linux kernel. He wrote the program specifically for the hardware he was using and independent of an operating system because he wanted to use the functions of his new PC with an 80386 processor. Development was done on MINIX using the GNU C Compiler."
    val actualResult = AdjustText.adjustText(text, 40)
    val expectedResult =
      s"""In 1991, while studying computer science
         |at University of Helsinki, Linus
         |Torvalds began a project that later
         |became the Linux kernel. He wrote the
         |program specifically for the hardware he
         |was using and independent of an
         |operating system because he wanted to
         |use the functions of his new PC with an
         |80386 processor. Development was done on
         |MINIX using the GNU C Compiler.""".stripMargin

    assertEquals(actualResult, expectedResult)
  }

  test("should adjust task text, preserving existing lines") {
    val text =
      """In 1991, while
        |studying computer
        |
        |science at University of Helsinki, Linus Torvalds began a project that later became the Linux kernel. He wrote the program specifically for the hardware he was using and independent of an operating system because he wanted to use the functions of his new PC with an 80386 processor. Development was done on MINIX using the GNU C Compiler.""".stripMargin
    val actualResult = AdjustText.adjustText(text, 40)
    val expectedResult =
      s"""In 1991, while
         |studying computer
         |
         |science at University of Helsinki, Linus
         |Torvalds began a project that later
         |became the Linux kernel. He wrote the
         |program specifically for the hardware he
         |was using and independent of an
         |operating system because he wanted to
         |use the functions of his new PC with an
         |80386 processor. Development was done on
         |MINIX using the GNU C Compiler.""".stripMargin

    assertEquals(actualResult, expectedResult)
  }

  test("should adjust task text which contains word longer then line max length") {
    val text = "pneumonoultramicroscopicsilicovolcanoconiosis. In 1991, while studying computer science at University of Helsinki, Linus Torvalds began a project that later became the Linux kernel. He wrote the program specifically for the hardware he was using and independent of an operating system because he wanted to use the functions of his new PC with an 80386 processor. Development was done on MINIX using the GNU C Compiler."
    val actualResult = AdjustText.adjustText(text, 40)
    val expectedResult =
      s"""pneumonoultramicroscopicsilicovolcanoconiosis.
         |In 1991, while studying computer science
         |at University of Helsinki, Linus
         |Torvalds began a project that later
         |became the Linux kernel. He wrote the
         |program specifically for the hardware he
         |was using and independent of an
         |operating system because he wanted to
         |use the functions of his new PC with an
         |80386 processor. Development was done on
         |MINIX using the GNU C Compiler.""".stripMargin

    assertEquals(actualResult, expectedResult)
  }

  test("should adjust empty string") {
    assertEquals(AdjustText.adjustText("", 40), "")
  }

  test("should adjust string with whitespaces") {
    val actualResult = AdjustText.adjustText("     ", 40)
    val expectedResult = ""
    assertEquals(actualResult, expectedResult)
  }
}
